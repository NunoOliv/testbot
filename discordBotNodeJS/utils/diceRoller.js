module.exports = {
  rollDice: function (numberOfDies, numberOfFaces) {
    const rolls = []

    for (let i = 0; i < numberOfDies; i++) {
      rolls.push(this.rollDie(numberOfFaces))
    }

    return rolls
  },

  rollDie: function (numberOfFaces) {
    return Math.floor(Math.random() * numberOfFaces + 1)
  },

  rollHighest: function (numberOfDies, numberOfFaces, highest) {
    const rolls = this.rollDice(numberOfDies, numberOfFaces)
    const finalRolls = highest(rolls, highest)
    return {
      rolls: finalRolls,
      log: finalRolls
    }
  },

  rollLowest: function (numberOfDies, numberOfFaces, lowest) {
    const rolls = this.rollDice(numberOfDies, numberOfFaces)
    const finalRolls = lowest(rolls, lowest)
    return {
      rolls: finalRolls,
      log: finalRolls
    }
  }
}

function highest (rolls, highest) {
  const rollValues = [...rolls]
  rollValues.sort((a, b) => b - a)
  rollValues.splice(0, highest)
  rollValues.sort(() => Math.random() - 0.5)
  return rollValues
}

function lowest (rolls, lowest) {
  const rollValues = [...rolls]
  rollValues.sort((a, b) => b - a)
  rollValues.splice(rollValues.length - lowest, -1)
  rollValues.sort(() => Math.random() - 0.5)
  return rollValues
}

function createLog(finalRolls, originalRolls) {
  if (!originalRolls) {
    return '(' + finalRolls.join(' ') + ')'
  } else {
    // TODO: create log for the highest and lowest case
  }
}