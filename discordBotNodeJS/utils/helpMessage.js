const fs = require('fs')
const Discord = require('discord.js')

module.exports = {

  buildMessage: function (filePath, comPrefix, title, description) {
    return new Promise((resolve, reject) => {
      fs.readFile(filePath, (error, data) => {
        if (error) {
          reject(error)
          return
        }
        try {
          const commandList = JSON.parse(data)

          const embedMessage = new Discord.MessageEmbed()
            .setColor('#ff0000')
            .setTitle(title)
            .setDescription(description)

          commandList.category.forEach(category => {
            let categoryDesc = ''
            category.commands.forEach(command => {
              categoryDesc = categoryDesc + '**' + comPrefix + command.name + '**' + ' - ' + command.description + '\n'
            })
            embedMessage.addField(category.name, categoryDesc, false)
          })

          resolve(embedMessage)
        } catch (error) {
          reject(error)
        }
      })
    })
  }

}
