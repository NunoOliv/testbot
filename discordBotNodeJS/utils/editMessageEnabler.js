const { loggers } = require('winston')
const db = require('../models')

var logger

module.exports = function (msg, args) {
  logger = loggers.get(process.env.logger)

  if (!msg.guild) {
    msg.reply('You can only use this command on a Server')
    return
  }
  if (!(args && Array.isArray(args) && args.length > 0) || args[0].length === 0) {
    msg.reply('You need to send an argument with this command')
    return
  }
  if (args.length !== 1) {
    msg.reply('You can only supports the enable flag')
    return
  }

  const enableFlag = args[0]

  if (enableFlag !== 'true' && enableFlag !== 'false') {
    msg.reply('You sent an invalid flag. Please use *true* or *false*')
    return
  }

  db.globalVars.findOne(
    {
      where:
      {
        varID: 'enableEdit',
        groupID: 'general',
        status: 'active',
        guild: msg.guild.id
      }
    }
  )
    .then(result => {
      if (result) {
        result.update({
          value: enableFlag
        }).then(() => {
          msg.client.editConfigs.set(msg.guild.id, enableFlag)
          msg.reply('Edit message warning set to: ' + enableFlag)
        }).catch(error => {
          msg.reply('Something went wrong! Unable to change command')
          logger.error(error)
        })
      } else {
        db.globalVars.create({
          varID: 'enableEdit',
          groupID: 'general',
          value: enableFlag,
          status: 'active',
          guild: msg.guild.id
        }).then(() => {
          msg.client.editConfigs.set(msg.guild.id, enableFlag)
          msg.reply('Edit message warning set to: ' + enableFlag)
        }).catch(error => {
          msg.reply('Something went wrong! Unable to change command')
          logger.error(error)
        })
      }
    })
    .catch(error => {
      msg.reply('Something went wrong! set flag')
      logger.error(error)
    })
}
