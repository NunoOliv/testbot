require('dotenv').config({ path: '../.env' })
const db = require('../models')
const covidAPI = require('../commands/covid/covidPT')
const { loggers } = require('winston')

var bot, logger

module.exports = {
  sendCovidNotification: function (discordBot) {
    logger = loggers.get(process.env.logger)
    logger.info('Initiating covid pm broadcast')
    bot = discordBot
    handleCovidSubscriptions()
  }
}

async function handleCovidSubscriptions () {
  Promise.all([
    getSubscribers(),
    getLastDay()
  ])
    .then((resp) => {
      const subscribeList = resp[0]
      const lastDay = resp[1]

      const currentDay = new Date().getDate()

      if (currentDay === lastDay) {
        logger.info('Latest Covid Data was already sent')
        bot.destroy()
        return
      }

      covidAPI.sendPMs(bot, subscribeList, lastDay)
        .then(response => {
          bot.destroy()
          if (typeof response === 'string') {
            logger.info(response)
            return
          }
          logger.info('Covid pms sent')
          const responseDay = response.data.split('-')[0]
          db.globalVars.update(
            {
              value: responseDay
            },
            {
              where: {
                varID: 'lastDay',
                groupID: 'covid',
                status: 'active'
              }
            })
            .then((resp) => {
              if (resp) logger.info('Last covid data day updated to ' + responseDay)
            })
            .catch((error) => {
              logger.error('Unable to update covid last day')
              logger.error(error)
            })
        })
        .catch(error => {
          bot.destroy()
          logger.error('Failed to send Covid PMs')
          logger.error(error)
        })
    })
    .catch(() => {
      logger.error('Failed to retrieve Covid Pm data requisites')
    })
}

function getSubscribers () {
  return new Promise((resolve, reject) => {
    db.Subscriptions.findAll({
      where: {
        subID: 'covid',
        status: 'active'
      }
    })
      .then((subscriptions) => {
        logger.info('Subscriber list retrieved successfully')
        const subsList = []
        subscriptions.forEach(sub => {
          subsList.push(sub.userID)
        })
        resolve(subsList)
      })
      .catch((error) => {
        logger.error('Failed to get Subscribers')
        logger.error(error)
        reject(error)
      })
  })
}

function getLastDay () {
  return new Promise((resolve, reject) => {
    db.globalVars.findAll({
      where: {
        varID: 'lastDay',
        groupID: 'covid',
        status: 'active'
      }
    })
      .then((resp) => {
        if (resp && resp.length === 1) {
          resolve(resp[0].value)
          logger.info('Covid PM latest sent day retrieved Successfully')
        } else {
          const errorMessage = new Error('More than one lastDay configuration')
          logger.error(errorMessage)
          reject(errorMessage)
        }
      })
      .catch((error) => {
        logger.error('Failed to get Covid PM Last Day')
        logger.error(error)
        reject(error)
      })
  })
}
