const { loggers } = require('winston')
const db = require('../models')

var logger

module.exports = {
  setPrefix: function (msg, args) {
    logger = loggers.get(process.env.logger)

    if (!msg.guild) {
      msg.reply('You can only use this command on a Server')
      return
    }
    if (!(args && Array.isArray(args) && args.length > 0) || args[0].length === 0) {
      msg.reply('You need to send a prefix with this command')
      return
    }
    if (args.length !== 1) {
      msg.reply('You can only send one prefix with this command')
      return
    }

    db.globalVars.findOne(
      {
        where:
        {
          varID: 'prefix',
          groupID: 'general',
          status: 'active',
          guild: msg.guild.id
        }
      }
    )
      .then(result => {
        if (result) {
          result.update({
            value: args[0]
          }).then(() => {
            msg.client.prefixes.set(msg.guild.id, args[0])
            msg.reply('Bot command prefix changed to: ' + args[0])
          }).catch(error => {
            msg.reply('Something went wrong! Unable to change command')
            logger.error(error)
          })
        } else {
          db.globalVars.create({
            varID: 'prefix',
            groupID: 'general',
            value: args[0],
            status: 'active',
            guild: msg.guild.id
          }).then(() => {
            msg.client.prefixes.set(msg.guild.id, args[0])
            msg.reply('Bot command prefix changed to: ' + args[0])
          }).catch(error => {
            msg.reply('Something went wrong! Unable to change command')
            logger.error(error)
          })
        }
      })
  },

  resetPrefix: function (msg) {
    logger = loggers.get(process.env.logger)

    if (!msg.guild) {
      msg.reply('You can only use this command on a Server')
      return
    }

    db.globalVars.findOne(
      {
        where:
        {
          varID: 'prefix',
          groupID: 'general',
          status: 'active',
          guild: msg.guild.id
        }
      }
    )
      .then(result => {
        if (result) {
          result.destroy()
            .then(resp => {
              if (resp) {
                msg.client.prefixes.delete(msg.guild.id)
                msg.reply('Prefix was reset!')
              } else {
                msg.reply('Failed to reset the prefix')
                logger.error('Failed to delete prefix from the database')
              }
            })
            .catch(error => {
              msg.reply('Failed to reset the prefix')
              logger.error('Failed to delete prefix from the database')
              logger.error(error)
            })
        } else {
          msg.reply('The prefix is already the default one')
        }
      })
      .catch(error => {
        msg.reply('Failed to reset the prefix')
        logger.error('Failed to retrieve prefix from the database')
        logger.error(error)
      })
  }
}
