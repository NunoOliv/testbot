'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class globalVars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
    }
  };
  globalVars.init({
    varID: DataTypes.STRING,
    groupID: DataTypes.STRING,
    value: DataTypes.STRING,
    status: DataTypes.STRING,
    guild: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'globalVars'
  })
  return globalVars
}
