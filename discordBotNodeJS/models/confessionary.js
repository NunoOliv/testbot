'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class confessionary extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
    }
  };
  confessionary.init({
    gID: DataTypes.STRING,
    confessChannelID: DataTypes.STRING,
    confessionChannelID: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'confessionary'
  })
  return confessionary
}
