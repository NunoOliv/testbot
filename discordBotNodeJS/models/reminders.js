'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class reminders extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
    }
  };
  reminders.init({
    userID: DataTypes.STRING,
    channelID: DataTypes.STRING,
    targetDate: DataTypes.DATE,
    message: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'reminders'
  })
  return reminders
}
