const { format, loggers, transports } = require('winston')

const alignedWithColorsAndTime = format.combine(
  format.timestamp(),
  format.align(),
  format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
)

const loggerDef = loggers.add('default-logger', {
  level: 'info',
  format: alignedWithColorsAndTime,
  defaultMeta: { service: 'default-service' },
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new transports.File({ filename: './logs/error.log', level: 'error' }),
    new transports.File({ filename: './logs/combined.log' })
  ]
})

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  loggerDef.add(new transports.Console({
    format: format.simple()
  }))
}

const loggerCov = loggers.add('covidPM-logger', {
  level: 'info',
  format: alignedWithColorsAndTime,
  defaultMeta: { service: 'covidPM-service' },
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new transports.File({ filename: './logs/covid_error.log', level: 'error' }),
    new transports.File({ filename: './logs/covid_combined.log' })
  ]
})

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  loggerCov.add(new transports.Console({
    format: format.simple()
  }))
}

module.exports = loggers
