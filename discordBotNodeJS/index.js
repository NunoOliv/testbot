require('dotenv').config()
const loggers = require('./winstonConfig')
var db = require('./models')
const Discord = require('discord.js')
const bot = new Discord.Client()
const botCommands = require('./commands')

const TOKEN = process.env.DISCORD_TOKEN
var logger
var loggerName

bot.commands = new Discord.Collection()
bot.prefixes = new Map()
bot.editConfigs = new Map()

switch (process.argv[2]) {
  case 'covidSubs':
    loggerName = 'covidPM-logger'
    process.env.logger = loggerName
    logger = loggers.get(loggerName)
    logger.info('Running in mode: Covid Subscriptions')
    bot.login(TOKEN).then(() => {
      require('./utils/covidPMs').sendCovidNotification(bot)
    }).catch((error) => {
      logger.error('Failed to Login at discord')
      logger.error(error)
    })
    break
  default:
    loggerName = 'default-logger'
    process.env.logger = loggerName
    logger = loggers.get(loggerName)
    db.sequelize.options.logging = (...msg) => logger.info(msg)
    logger.info('Running in mode: default')
    configuration().then(() => {
      activateCommands()
    })
}

async function configuration () {
  logger.verbose('Starting bot configuration')
  configLogger()
  try {
    await configPrefix()
    await configEdit()
    await configBot()
    await configMusicPlayer()
    await confessConfig()
  } catch (error) {
    logger.error(error)
  }
}

function configPrefix () {
  logger.verbose('Configuring command prefix')
  return new Promise((resolve, reject) => {
    db.globalVars.findAll({
      where: {
        varID: 'prefix',
        groupID: 'general',
        status: 'active'
      }
    })
      .then((prefixes) => {
        if (prefixes && Array.isArray(prefixes)) {
          prefixes.forEach(prefix => {
            bot.prefixes.set(prefix.guild, prefix.value)
          })
          logger.info('Command prefixes set')
          resolve()
        } else {
          reject(new Error('Invalid prefixes db response'))
        }
      })
      .catch((error) => {
        reject(new Error('Error getting prefix from database ' + error))
      })
  })
}

function configEdit () {
  logger.verbose('Configuring message update')
  return new Promise((resolve, reject) => {
    db.globalVars.findAll({
      where: {
        varID: 'enableEdit',
        groupID: 'general',
        status: 'active'
      }
    })
      .then((editConfigs) => {
        if (editConfigs && Array.isArray(editConfigs)) {
          editConfigs.forEach(editConfig => {
            bot.editConfigs.set(editConfig.guild, editConfig.value)
          })
          logger.info('Edit flags set')
          resolve()
        } else {
          reject(new Error('Invalid edit flags db response'))
        }
      })
      .catch((error) => {
        reject(new Error('Error getting edit flags from database ' + error))
      })
  })
}

function configBot () {
  return new Promise((resolve, reject) => {
    setCommands(botCommands)
    bot.login(TOKEN).then(() => {
      logger.info(`Login successfully as ${bot.user.tag}!`)
      resolve()
    }).catch((error) => {
      reject(new Error('Login failed' + error))
    })
  })
}

function setCommands (commands) {
  Object.keys(commands).map(key => {
    if (commands[key].command) {
      bot.commands.set(commands[key].name, commands[key])
    } else {
      setCommands(commands[key])
    }
  })
}

function confessConfig () {
  return new Promise((resolve, reject) => {
    db.confessionary.findAll(
      {
        where: {
          status: 'active'
        }
      })
      .then((confessionaries) => {
        global.confessionaryList = confessionaries
        logger.info('Confessionary information retrieved')
        resolve()
      })
      .catch(error => {
        reject(new Error('Failed to retrieve confessionary information' + error))
      })
  })
}

function activateCommands () {
  bot.on('ready', () => {
    logger.info('Bot Ready!')
  })

  bot.on('message', msg => {
    if (msg.author.bot) return

    let guildID = '0'

    if (msg.guild) {
      guildID = msg.guild.id
    }

    const prefix = msg.client.prefixes.get(guildID) || msg.client.prefixes.get('0')

    let messageContent = msg.content

    if (messageContent.indexOf(prefix) === 0) {
      messageContent = messageContent.slice(prefix.length)
    } else {
      return
    }

    const args = messageContent.split(/ +/)
    var command = args.shift().toLowerCase()

    if (msg.guild) {
      const confessSettings = global.confessionaryList.find(conf => conf.gID === msg.guild.id)
      const confessChannelID = confessSettings ? confessSettings.confessChannelID : null

      if (msg.channel.id === confessChannelID) {
        command = 'confess'
        args.unshift('message')
      }
    }

    if (!bot.commands.has(command)) return

    try {
      logger.info(`Called command: ${command}`)
      bot.commands.get(command).execute(msg, args)
    } catch (error) {
      logger.error(error)
      msg.reply('there was an error trying to execute that command!')
    }
  })

  bot.on('messageUpdate', (oldMessage, newMessage) => {
    logger.info('A message was updated')
    let guildID = 0

    if (oldMessage.guild) {
      guildID = oldMessage.guild.id
    } else {
      return
    }
    const editFlag = oldMessage.client.editConfigs.get(guildID) || 'false'

    if (editFlag !== 'true') {
      return
    }

    if (oldMessage.author.bot) return
    if (oldMessage.embeds.length === 0 && newMessage.embeds.length === 1) return
    try {
      newMessage.reply('I SAW YOU!!!! -> "' + oldMessage.content + '" edited into: "' + newMessage.content + '"')
    } catch (error) {
      logger.error('Failed editing message')
      logger.error(error)
      oldMessage.reply('there was an error trying to execute that command!')
    }
  })

  bot.on('warn', (info) => logger.warn(info))
  bot.on('error', (error) => logger.error(error))
}

function configLogger () {
  process.on('exit', (code) => {
    logger.info('Program exited with code: ' + code)
  })
}

function configMusicPlayer () {
  bot.queue = new Map()
}
