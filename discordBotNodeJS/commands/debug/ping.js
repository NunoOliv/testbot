const { loggers } = require('winston')
module.exports = {
  name: 'ping',
  description: 'Ping!',
  command: true,
  execute (msg) {
    const logger = loggers.get(process.env.logger)
    logger.info('Ping - Pong')
    msg.reply('pong')
  }
}
