module.exports = {
  CovidPT: require('./covid'),
  Debug: require('./debug'),
  General: require('./general'),
  GW2: require('./gw2'),
  musicBot: require('./musicBot')
}
