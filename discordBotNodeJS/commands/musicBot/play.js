const { loggers } = require('winston')
const ytdl = require('ytdl-core-discord')
const YouTubeAPI = require('simple-youtube-api')
const youtube = new YouTubeAPI(process.env.YOUTUBE_TOKEN)

var logger
module.exports = {
  name: 'play',
  description: 'play youtube songs!',
  command: true,
  async execute (msg, args) {
    logger = loggers.get(process.env.logger)
    const serverQueue = msg.client.queue.get(msg.guild.id)
    const voiceChannel = msg.member.voice.channel

    if (!voiceChannel) return msg.channel.send('You need to be in a voice channel to play music!')
    const permissions = voiceChannel.permissionsFor(msg.client.user)

    if (!permissions.has('CONNECT') || !permissions.has('SPEAK')) {
      return msg.channel.send('I need the permissions to join and speak in your voice channel!')
    }
    if (!args.length) {
      msg.reply('How can I play if there is nothing to play!')
      return
    }

    let videoUrl = args[0]
    const search = args.join(' ')
    const videoPattern = /^(https?:\/\/)?(www\.)?(m\.)?(youtube\.com|youtu\.?be)\/.+$/gi
    const urlValid = videoPattern.test(videoUrl)
    let songInfo = null
    let song = null

    if (!urlValid) {
      const results = await youtube.searchVideos(search, 1)
      videoUrl = results[0].url
    }

    try {
      songInfo = await ytdl.getInfo(videoUrl)
      song = {
        title: songInfo.title,
        url: songInfo.video_url,
        duration: songInfo.lengthSeconds
      }
    } catch (error) {
      let errorMessage = null
      if (urlValid) {
        errorMessage = 'Failed to get url information from youtube downloader'
      } else {
        errorMessage = 'Failed to get search query information from youtube downloader'
      }
      msg.reply(errorMessage).catch(error => logger.error(error))
      logger.error(errorMessage)
      logger.error(error)
    }

    logger.info('Trying to play: ' + song.title)

    if (!serverQueue) {
      msg.channel.send('Playing: ' + song.url)

      const queueStructure = {
        textChannel: msg.channel,
        voiceChannel: voiceChannel,
        connection: null,
        songs: [],
        volume: 5,
        playing: true
      }

      msg.client.queue.set(msg.guild.id, queueStructure)

      queueStructure.songs.push(song)

      try {
        const connection = await voiceChannel.join()
        queueStructure.connection = connection
        play(msg.guild, queueStructure.songs[0])
      } catch (error) {
        logger.error(error)
        serverQueue.delete(msg.guild.id).catch(error => logger.catch(error))
        return msg.channel.send(error).catch(error => logger.catch(error))
      }
    } else {
      msg.channel.send('Added to queue: ' + song.url)
      serverQueue.songs.push(song)
      logger.info(serverQueue.songs)
      return msg.channel.send(`${song.title} has been added to the queue!`).catch(error => logger.catch(error))
    }
  }
}

async function play (guild, song) {
  const serverQueue = guild.client.queue.get(guild.id)

  if (!song) {
    serverQueue.voiceChannel.leave()
    guild.client.queue.delete(guild.id)
  } else {
    try {
      const dispatcher = serverQueue.connection.play(await ytdl(song.url), { type: 'opus' })
        .on('finish', () => {
          const lastPlayed = serverQueue.songs.shift()
          logger.info('Finished playing: ' + lastPlayed)
          play(guild, serverQueue.songs[0])
        })
        .on('error', error => {
          logger.error(error)
        })
      dispatcher.setVolumeLogarithmic(serverQueue.volume / 5)
    } catch (error) {
      logger.error(error)
      serverQueue.delete(guild.id).catch(error => logger.catch(error))
    }
  }
}
