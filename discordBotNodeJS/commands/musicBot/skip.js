const { loggers } = require('winston')
let logger

module.exports = {
  name: 'skip',
  description: 'play youtube songs!',
  command: true,
  execute (msg) {
    logger = loggers.get(process.env.logger)
    const serverQueue = msg.client.queue.get(msg.guild.id)

    if (!msg.member.voice.channel) return msg.channel.send('You have to be in a voice channel to stop the music!')
    if (!serverQueue) return msg.channel.send('There is no song that I could skip!')
    try {
      serverQueue.connection.dispatcher.end()
    } catch (error) {
      logger.error('Failed to end music stream on skip')
      logger.error(error)
    }
  }
}
