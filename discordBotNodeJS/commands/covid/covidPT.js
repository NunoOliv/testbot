const axios = require('axios')
const Discord = require('discord.js')
const dgsURL = process.env.DGS_URL
const dgsLastUpdateEndpoint = process.env.DGS_LAST_UPDATE
const dgsGetEntryEndpoint = process.env.DGS_GET_ENTRY
const { loggers } = require('winston')
var logger

module.exports = {
  name: 'covid',
  description: 'Dados COVID19 Portugal',
  command: true,
  execute (msg, args) {
    logger = loggers.get(process.env.logger)
    getCovidData((data) => {
      var formattedMessage
      try {
        formattedMessage = beautifyResponse(data)
      } catch (error) {
        logger.error('Failed to format message')
        logger.error(error)
        formattedMessage = 'Error formatting message'
      }
      if (args[0] === 'pm') {
        msg.member.send(formattedMessage).then(() => {
          logger.info('Covid Pm sent')
        })
      } else {
        msg.channel.send(formattedMessage).then(() => {
          logger.info('Covid info sent')
        })
      }
    },
    (error) => {
      logger.error('Failed to get covid Data')
      logger.error(error)
    })
  },

  async sendPMs (client, userIDs, lastDay) {
    logger = loggers.get(process.env.logger)
    return new Promise((resolve, reject) => {
      getCovidData((data) => {
        const day = data.data.split('-')[0]
        if (day === lastDay) {
          resolve('Data already PMed')
          logger.info('No new data to send')
          return
        }

        const promises = []

        userIDs.forEach(async userID => {
          try {
            const bResponse = prepareSendCovidMsgPromise(userID, client, data)
            promises.push(bResponse)
          } catch (error) {
            logger.error('Could not find the user')
            logger.error(error)
          }
        })

        Promise.all(promises).then(() => {
          resolve(data)
          logger.info('PMs sent successfully')
        }).catch(error => {
          logger.error('Failed to send covid PMs')
          logger.error(error)
          reject(error)
        })
      },
      (error) => {
        logger.error('Failed to get covid subscription list')
        logger.error(error)
        reject(error)
      })
    })
  }
}

async function prepareSendCovidMsgPromise (userID, client, data) {
  const user = await client.users.fetch(userID)
  return user.send(beautifyResponse(data))
}

function getCovidData (sCallback, fCallback) {
  const lastUpdateURI = [dgsURL, dgsLastUpdateEndpoint].join('/')

  axios.get(lastUpdateURI)
    .then(resp => {
      var yesterday = getPreviousDay(resp.data.data)
      const getEntryURI = [dgsURL, dgsGetEntryEndpoint, yesterday].join('/')
      axios.get(getEntryURI)
        .then(resp2 => {
          var dayOfYear = Object.keys(resp2.data.data)[0]
          resp.data.recuperados_novos = resp.data.recuperados - resp2.data.recuperados[dayOfYear]
          resp.data.obitos_novos = resp.data.obitos - resp2.data.obitos[dayOfYear]
          resp.data.internados_novos = resp.data.internados - resp2.data.internados[dayOfYear]
          resp.data.internados_uci_novos = resp.data.internados_uci - resp2.data.internados_uci[dayOfYear]
          sCallback(resp.data)
        })
        .catch(error => {
          logger.error('Failed to get covid Data')
          logger.error(error)
          fCallback(error)
        })
    })
    .catch(error => {
      logger.error('Failed to get covid data')
      logger.error(error)
      fCallback(error)
    })
}

function beautifyResponse (data) {
  const embedMessage = new Discord.MessageEmbed()
    .setColor('#ff0000')
    .setTitle('Covid em Portugal')
    .setDescription('Dados da DGS')
    .setThumbnail('https://i.imgur.com/wnfwJRx.png')
    .addFields(
      { name: 'Data', value: data.data, inline: false },
      { name: '\u200B', value: '\u200B' },
      { name: 'Confirmados', value: data.confirmados, inline: true },
      { name: 'Confirmados Novos', value: data.confirmados_novos, inline: true },
      { name: '\u200B', value: '\u200B' },
      { name: 'Recuperados', value: data.recuperados, inline: true },
      { name: 'Recuperados Novos', value: data.recuperados_novos, inline: true },
      { name: '\u200B', value: '\u200B' },
      { name: 'Internados', value: data.internados, inline: true },
      { name: 'Diferença de Internados', value: data.internados_novos, inline: true },
      { name: '\u200B', value: '\u200B' },
      { name: 'Internados UCI', value: data.internados_uci, inline: true },
      { name: 'Diferença de Internados UCI', value: data.internados_uci_novos, inline: true },
      { name: '\u200B', value: '\u200B' },
      { name: 'Obitos', value: data.obitos, inline: true },
      { name: 'Obitos Novos', value: data.obitos_novos, inline: true }
    )
    .setTimestamp()
    .setFooter(process.env.DGS_URL, 'https://i.imgur.com/wnfwJRx.png')

  return embedMessage
}

function getPreviousDay (date) {
  var splitDate = date.split('-')
  var dateObject = new Date([splitDate[1], splitDate[0], splitDate[2]].join('/'))
  var yesterday = new Date(dateObject - 1)

  return ('0' + yesterday.getDate()).slice(-2) + '-' + ('0' + (yesterday.getMonth() + 1)).slice(-2) + '-' + yesterday.getFullYear()
}
