const db = require('../../models')
const { loggers } = require('winston')
module.exports = {
  name: 'subcovid',
  description: 'subcovid!',
  command: true,
  execute (msg) {
    const memberID = msg.member.id

    const logger = loggers.get(process.env.logger)
    db.Subscriptions.findAll({
      where: {
        subID: 'covid',
        userID: memberID,
        status: 'active'
      }
    })
      .then((subscriptions) => {
        if (subscriptions && subscriptions.length >= 1) {
          msg.reply('You are already subscribed!')
          logger.info('Member: ' + msg.member.user.username + '#' + msg.member.user.discriminator + ' was already subscribed')
        } else {
          db.Subscriptions.create(
            {
              subID: 'covid',
              userID: memberID,
              status: 'active'
            }
          )
            .then(() => {
              msg.reply('You are subscribed!')
              logger.info('Member: ' + msg.member.user.username + '#' + msg.member.user.discriminator + ' subscribed')
            })
            .catch((error) => {
              msg.reply('Sorry I wasn\'t able to preform the subscription')
              logger.error('Failed to create covid subscription')
              logger.error(error)
            })
        }
      })
      .catch(error => {
        logger.error('Failed to get covid subscription')
        logger.error(error)
      })
  }
}
