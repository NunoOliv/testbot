const db = require('../../models')
const { loggers } = require('winston')

module.exports = {
  name: 'unsubcovid',
  description: 'unsubcovid!',
  command: true,
  execute (msg) {
    const memberID = msg.member.id
    const logger = loggers.get(process.env.logger)

    db.Subscriptions.destroy({
      where: {
        subID: 'covid',
        userID: memberID,
        status: 'active'
      }
    })
      .then((resp) => {
        if (resp > 0) {
          msg.reply('You are unsubscribed!')
          logger.info('Member: ' + msg.member.user.username + '#' + msg.member.user.discriminator + ' was unsubscribed')
        } else {
          msg.reply('You are not subscribed!')
          logger.info('Member: ' + msg.member.user.username + '#' + msg.member.user.discriminator + ' was not subscribed')
        }
      })
      .catch(error => {
        logger.error('Failed to delete subscription')
        logger.error(error)
      })
  }
}
