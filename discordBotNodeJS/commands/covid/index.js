module.exports = {
  CovidPT: require('./covidPT'),
  SubCovid: require('./subscribeCovid'),
  UnsubCovid: require('./unsubscribeCovid')
}
