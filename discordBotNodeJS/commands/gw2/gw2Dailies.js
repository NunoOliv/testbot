const axios = require('axios')
const Discord = require('discord.js')
const { loggers } = require('winston')
const url = process.env.GW2_URL
const version = process.env.GW2_API_VERSION
const dailyEndpoint = process.env.GW2_DAILY
const achievementsEndpoint = process.env.GW2_ACHIEVEMENTS

var logger

module.exports = {
  name: 'gw2daily',
  description: 'Guild wars 2 dailies',
  command: true,
  execute (msg, args) {
    // URI for daily
    const dailyURI = [url, version, dailyEndpoint].join('/')

    logger = loggers.get(process.env.logger)

    axios.get(dailyURI)
      .then(resp => {
        var filteredResp = {}

        if (args.length === 0) {
          filteredResp = resp.data
        } else {
          args.forEach(element => {
            if (resp.data[element]) {
              filteredResp[element] = resp.data[element]
            }
          })
        }
        const cleanDaily = cleanDailyList(filteredResp)

        getDailyDetails(cleanDaily)
          .then(message => {
            msg.channel.send(message)
              .then(() => {
                logger.info('Gw2 dailies sent to channel ' + msg.channel.name)
              })
              .catch(error => {
                logger.error('Gw2 dailies failed')
                logger.error(error)
              })
          })
          .catch(error => {
            logger.error('Failed to get Error Details')
            logger.error(error)
          })
      })
      .catch(error => {
        logger.error('Failed to get Daily info')
        logger.error(error)
      })
  }
}

function cleanDailyList (dailyList) {
  var cleanDailyList = dailyList
  for (const property in cleanDailyList) {
    cleanDailyList[property].forEach(element => {
      if (element.level.max < 80) {
        const index = cleanDailyList[property].indexOf(element)
        cleanDailyList[property].splice(index, 1)
      }
    })
  }
  return cleanDailyList
}

function getDailyDetails (dailyList) {
  return new Promise((resolve, reject) => {
    const promises = []

    for (const category in dailyList) {
      promises.push(getDailyCategoryDetails(category, dailyList[category]))
    }
    Promise.all(promises)
      .then(data => {
        resolve(beautifyResponse(data))
      })
      .catch(error =>
        reject(error)
      )
  })
}

function getDailyCategoryDetails (category, dailyList) {
  return new Promise((resolve, reject) => {
    const ids = []
    dailyList.forEach(daily => {
      ids.push(daily.id)
    })
    if (dailyList.length <= 0) {
      resolve()
      return
    }
    const dailyURI = [url, version, achievementsEndpoint].join('/')
    const idsString = ids.join(',')

    axios.get(dailyURI,
      {
        params: {
          ids: idsString
        }
      })
      .then(resp => {
        var detailedDaily = {}
        detailedDaily[category] = resp.data
        resolve(detailedDaily)
      })
      .catch(error =>
        reject(error)
      )
  })
}

function beautifyResponse (dailyList) {
  try {
    const embedMessage = new Discord.MessageEmbed()
      .setColor('#ff0000')
      .setTitle('Guild Wars 2 Daily\'s')
      .setDescription('Daily GW2 missions')
      .setThumbnail('https://wiki.guildwars.com/images/b/bf/Normal_gw2logo.jpg')

    dailyList.forEach(category => {
      if (!category) return
      const categoryName = Object.keys(category)[0]
      var inlineFlag = false
      if (category[categoryName].length > 4) {
        inlineFlag = true
      } else if (category[categoryName].length <= 0) {
        return
      }
      embedMessage.addFields(
        { name: '\u200B', value: '\u200B' },
        { name: categoryName.toUpperCase(), value: 'Category' },
        { name: '\u200B', value: '\u200B' }
      )

      category[categoryName].forEach(daily => {
        if (!daily.requirement) {
          daily.requirement = 'No Description'
        }
        embedMessage.addField(
          daily.name,
          daily.requirement,
          inlineFlag
        )
      })
    }
    )

    embedMessage.setTimestamp()

    return embedMessage
  } catch (error) {
    logger.error('Failed to format GW2 daily message')
    logger.error(error)

    return "Sorry, couldn't show GW2 dailies"
  }
}
