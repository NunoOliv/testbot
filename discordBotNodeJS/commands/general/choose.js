const { loggers } = require('winston')
let logger

module.exports = {
  name: 'choose',
  description: 'choose x from list',
  command: true,
  execute (msg, args) {
    logger = loggers.get(process.env.logger)

    let nPicks = args.shift()
    let argSize = args.length

    if (isNaN(nPicks) || nPicks.indexOf('.') !== -1 || nPicks.indexOf(',') !== -1) {
      msg.reply('The first argument needs to be the number of elements you want to pick').catch(error => logger.error(error))
      return
    }
    nPicks = parseInt(nPicks)

    if (nPicks === 0) {
      msg.reply('the number of picks cant be Zero').catch(error => logger.error(error))
      return
    }

    if (argSize < nPicks) {
      msg.reply('You need to provide more elements than the number of picks you want').catch(error => logger.error(error))
      return
    }

    let responseList = ''

    for (let i = 0; i < nPicks; i++) {
      const position = Math.floor(Math.random() * argSize)

      const chosenElement = args[position]

      responseList = responseList.concat(chosenElement, ' ')

      args.splice(position, 1)

      argSize--
    }

    msg.reply(responseList).catch(logger.error)
  }
}
