const { loggers } = require('winston')
var moment = require('moment-timezone')

const suffixes = new Map()
suffixes.set('d', 86400000)
suffixes.set('h', 3600000)
suffixes.set('m', 60000)
suffixes.set('s', 1000)

const messageSeparator = 'to'
const defaultMorning = [7, 0]
const timezone = 'Europe/Lisbon'

module.exports = {
  name: 'remindme',
  description: 'Remainder',
  command: true,
  execute (msg, args) {
    var modifier = args.shift()

    const indexOfMessage = args.indexOf(messageSeparator)

    if (indexOfMessage === -1) {
      msg.reply('You need to add *' + messageSeparator + '* before the message')
      return
    }

    const message = args.slice(indexOfMessage + 1).join(' ')
    const timerArgs = args.slice(0, indexOfMessage)
    const userID = msg.author.id
    let time = 0
    switch (modifier) {
      case 'in':
        time = inTime(timerArgs)
        if (time instanceof Error) {
          loggers.error(time)
          msg.reply(time)
        } else if (!time) {
          msg.reply('Invalid date parameters')
        } else {
          setRemainder(message, time, msg.channel, userID)
          msg.reply('Remainder set!')
        }
        break
      case 'at':
        time = atTime(timerArgs)
        if (time instanceof Error) {
          loggers.error(time)
          msg.reply(time)
        } else if (!time) {
          msg.reply('Invalid date parameters')
        } else {
          setRemainder(message, time, msg.channel, userID)
          msg.reply('Remainder set!')
        }
        break
      default:
        msg.reply('The parameter ' + modifier + ' is not valid.')
    }
  }
}

function setRemainder (message, time, channel, userId) {
  setTimeout(() => {
    channel.send(`Reminder to <@${userId}>: ${message}`)
  }, time)
}

function inTime (args) {
  let counter = 0

  while (args.length > 0) {
    const arg = args.shift()
    const prefix = arg.slice(0, arg.length - 1)
    const suffix = arg.slice(arg.length - 1)

    if (isNaN(prefix) || !suffixes.has(suffix)) {
      return new Error('Invalid remainder parameters. Ex: 1d 12h 30m 5s')
    }
    const number = parseInt(prefix)

    counter = counter + (number * suffixes.get(suffix))
  }

  return counter
}

function atTime (args) {
  const dateRegEx = /^(20)\d\d-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$/
  const timeRegEx = /^(2[0-3]|[01]?[0-9]):([0-5][0-9])$/

  if (args.length > 2) {
    return new Error('Too many time parameters')
  }

  let dateCheck = false
  let timeCheck = false
  const currentDate = moment().tz(timezone)
  let timeArray
  let dateArray

  while (args.length > 0) {
    const arg = args.shift()
    if (dateRegEx.test(arg)) {
      if (dateCheck) return new Error('Cannot have more than one date')
      dateArray = arg.split('-')
      dateArray[1] = dateArray[1] - 1
      dateCheck = true
    } else if (timeRegEx.test(arg)) {
      if (timeCheck) return new Error('Cannot have more than one time')
      timeArray = arg.split(':')
      timeCheck = true
    } else {
      return new Error('Invalid Parameter found')
    }
  }
  if (!dateCheck) {
    dateArray = [currentDate.year(), currentDate.month(), currentDate.day()]
  }

  if (!timeCheck) {
    timeArray = defaultMorning
  }

  const targetDate = moment.tz(dateArray.concat(timeArray), timezone)
  const timeDifference = targetDate.diff(currentDate)

  if (timeDifference < 0) return new Error('I\' not a time machine! I can\'t send messages to the past!')
  return timeDifference
}
