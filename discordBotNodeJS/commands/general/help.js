const helpMessage = require('../../utils/helpMessage')
const { loggers } = require('winston')

module.exports = {
  name: 'help',
  description: 'help!',
  command: true,
  async execute (msg) {
    const logger = loggers.get(process.env.logger)
    const filePath = './config/commandList.json'
    const title = 'Help'
    const description = 'List of Commands'
    let guildID = '0'

    if (msg.guild) {
      guildID = msg.guild.id
    }

    const comPrefix = msg.client.prefixes.get(guildID) || msg.client.prefixes.get('0')
    try {
      const embedMessage = await helpMessage.buildMessage(filePath, comPrefix, title, description)
      msg.channel.send(embedMessage)
    } catch (error) {
      msg.reply('Unable to generate help message\n' + error)
      logger.error(error)
    }
  }
}
