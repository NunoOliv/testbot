const { loggers } = require('winston')
const prefixCommands = require('../../utils/adminPrefix')
const helpMessage = require('../../utils/helpMessage')
const setEditMessage = require('../../utils/editMessageEnabler')
var logger

module.exports = {
  name: 'admin',
  description: 'Private messages!',
  command: true,
  execute (msg, args) {
    logger = loggers.get(process.env.logger)

    if (!msg.guild) {
      msg.reply('This command can only be used in a Server')
      return
    }

    if (msg.member.hasPermission('ADMINISTRATOR')) {
      const setting = args.shift()
      if (!setting) {
        msg.reply('This command requires a parameter')
        return
      }
      switch (setting) {
        case 'setPrefix':
          prefixCommands.setPrefix(msg, args)
          break
        case 'resetPrefix':
          prefixCommands.resetPrefix(msg)
          break
        case 'setEditFlag':
          setEditMessage(msg, args)
          break
        case 'help':
          adminHelp(msg)
          break
        default:
          msg.reply('There is no admin command for ' + setting)
      }
    } else {
      logger.info('User ' + msg.member.user.username + '#' + msg.member.user.discriminator + ' tried to change settings without Administrator permissions.')
      msg.reply('Only users with admin privileges can use this command')
    }
  }
}

async function adminHelp (msg) {
  const filePath = './config/adminCommandList.json'
  const title = 'Admin Help'
  const description = 'List of Admin Commands'

  let guildID = '0'

  if (msg.guild) {
    guildID = msg.guild.id
  }

  const comPrefix = msg.client.prefixes.get(guildID) || msg.client.prefixes.get('0')
  try {
    const embedMessage = await helpMessage.buildMessage(filePath, comPrefix, title, description)
    msg.channel.send(embedMessage)
  } catch (error) {
    msg.reply('Unable to generate help message\n' + error)
    logger.error(error)
  }
}
