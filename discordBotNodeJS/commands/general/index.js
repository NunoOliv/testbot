module.exports = {
  Confess: require('./confess'),
  Help: require('./help'),
  Admin: require('./admin'),
  Remainder: require('./remainder'),
  choose: require('./choose'),
  roll: require('./roller'),
  roll_stats: require('./rollStats')
}
