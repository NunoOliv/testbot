// const { loggers } = require('winston')
const diceRoller = require('../../utils/diceRoller')

module.exports = {
  name: 'roll_stats',
  description: 'Roll character stats',
  command: true,
  execute (msg, args) {
    let stats
    let sum = 0

    let limited = true

    if (args.indexOf('unlimited') !== -1) {
      limited = false
    }
    if (limited) {
      while (sum > 80 || sum < 70) {
        stats = rollStats()
        sum = stats.reduce((a, b) => a + b, 0)
      }
    } else {
      stats = rollStats()
      sum = stats.reduce((a, b) => a + b, 0)
    }

    if (!args || args.indexOf('unsort') === -1) {
      stats.sort((a, b) => b - a)
    }

    msg.reply(statsReplyMessage(stats, sum))
  }
}

function rollStat () {
  let sum = 0
  const values = diceRoller.rollDice(4, 6)

  values.sort((a, b) => b - a)
  values.pop()
  sum = values.reduce((a, b) => a + b, 0)

  return sum
}

function statsReplyMessage (stats, sum) {
  const statsList = stats.join(' ')
  const messagePrefix = ' your stats are '
  const totalStatsMessage = 'Your total score is: '

  return messagePrefix + statsList + '\n' + totalStatsMessage + sum
}

function rollStats () {
  const stats = []

  for (let i = 0; i < 6; i++) {
    stats.push(rollStat())
  }

  return stats
}
