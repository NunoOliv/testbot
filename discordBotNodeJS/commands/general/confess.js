const db = require('../../models')
const { loggers } = require('winston')
var logger

module.exports = {
  name: 'confess',
  description: 'Private messages!',
  command: true,
  execute (msg, args) {
    logger = loggers.get(process.env.logger)
    if (!msg.guild) {
      msg.reply('Cannot use this command in private message')
      return
    }
    switch (args[0]) {
      case 'set':
        if (msg.member.hasPermission('ADMINISTRATOR')) {
          confessInit(msg, args[1])
        } else {
          logger.info('User ' + msg.member.user.username + '#' + msg.member.user.discriminator + ' tried to change confessionary settings without Administrator permissions.')
          msg.reply('Only users with admin privileges can change confessionary settings')
        }

        break
      case 'message':
        confessChannel(msg)
        break
      case 'pm':
        // confessPM(msg)
        msg.reply('Command Disabled')
    }
  },

  updateConfessionaries: function () {
    return new Promise((resolve) => {
      db.confessionary.findAll().then((confessionaries) => {
        global.confessionaryList = confessionaries
        resolve()
      })
    })
  }
}

function confessInit (msg, channelType) {
  const channelID = msg.channel.id
  const guildID = msg.guild.id
  const guildName = msg.guild.name
  const confessSettings = global.confessionaryList.find(conf => conf.gID === msg.guild.id)

  switch (channelType) {
    case 'confess':
      logger.info('Initializing confess channel configuration for guild: ' + guildName + ' : ' + msg.guild.id)
      if (confessSettings) {
        db.confessionary.update({ confessChannelID: channelID }, {
          where: {
            gID: guildID,
            status: 'active'
          }
        })
          .then(code => {
            if (code) {
              updateConfessionaries().then(() => msg.reply('Channel set as confession'))
              logger.info('Set confess channel configuration for guild: ' + guildName + ' : ' + guildID)
            }
          })
          .catch(error => {
            msg.reply('Failed to set channel as confess')
            logger.error('Failed confess channel configuration for guild: ' + guildName + ' : ' + guildID)
            logger.error(error)
          })
      } else {
        db.confessionary.create(
          {
            gID: guildID,
            confessChannelID: channelID,
            confessionChannelID: null,
            status: 'active'
          })
          .then(code => {
            if (code) {
              updateConfessionaries().then(() => msg.reply('Channel set as confession'))
              logger.info('Set confess channel configuration for guild: ' + guildName + ' : ' + guildID)
            }
          })
          .catch(error => {
            msg.reply('Failed to set channel as confess')
            logger.error('Failed confess channel configuration for guild: ' + guildName + ' : ' + guildID)
            logger.error(error)
          })
      }
      break
    case 'confessions':

      logger.info('Initializing confessions channel configuration for guild: ' + guildName + ' : ' + msg.guild.id)
      if (confessSettings) {
        db.confessionary.update({ confessionChannelID: channelID }, {
          where: {
            gID: guildID,
            status: 'active'
          }
        })
          .then(code => {
            if (code) {
              updateConfessionaries().then(() => msg.reply('Channel set as confession'))
              logger.info('Set confess channel configuration for guild: ' + guildName + ' : ' + guildID)
            }
          })
          .catch(error => {
            msg.reply('Failed to set channel as confession')
            logger.error('Failed confess channel configuration for guild: ' + guildName + ' : ' + guildID)
            logger.error(error)
          })
      } else {
        db.confessionary.create(
          {
            gID: guildID,
            confessChannelID: null,
            confessionChannelID: channelID,
            status: 'active'
          })
          .then(code => {
            if (code) {
              updateConfessionaries().then(() => msg.reply('Channel set as confession'))
              logger.info('Set confess channel configuration for guild: ' + guildName + ' : ' + guildID)
            }
          })
          .catch(error => {
            msg.reply('Failed to set channel as confess')
            logger.error('Failed confess channel configuration for guild: ' + guildName + ' : ' + guildID)
            logger.error(error)
          })
      }
      break
    default:
      msg.reply('Invalid init argument')
  }
}

function confessChannel (msg) {
  const channelID = msg.channel.id
  msg.delete().then(() => {
    logger.info('Confession Deleted')
    const confessSettings = global.confessionaryList.find(conf => conf.gID === msg.guild.id)

    if (channelID === confessSettings.confessChannelID) {
      const messageCont = msg.content
      const client = msg.client
      client.channels.fetch(confessSettings.confessionChannelID).then(channel => {
        sendConfession(channel, messageCont)
      })
    } else {
      msg.channel.send('You are using the wrong channel!!!')
    }
  }).catch(error => {
    logger.error('Unable to delete confession!')
    logger.error(error)
  })
}
/** Disabled
function confessPM (msg) {
  const messageCont = msg.content.replace('!confess pm ', '')
  const confessSettings = global.confessionaryList.find(conf => conf.gID === msg.guild.id)

  msg.client.channels.fetch(confessSettings.confessionChannelID).then(channel => {
    sendConfession(channel, messageCont)
  }).catch(error => {
    console.log(error)
  })
} */

function sendConfession (channel, messageCont) {
  channel.send({
    embed: {
      title: 'Confession',
      color: 3447003,
      description: messageCont
    }
  }).then(() => {
    logger.info('Confession sent successfully')
  }).catch(error => {
    logger.error('Failed sending a confession')
    logger.error(error)
  })
}

function updateConfessionaries () {
  return new Promise((resolve, reject) => {
    db.confessionary.findAll(
      {
        where: {
          status: 'active'
        }
      })
      .then((confessionaries) => {
        global.confessionaryList = confessionaries
        resolve()
        logger.info('Confessionaries updated')
      })
      .catch(error => {
        logger.error('Failed to retrieve updated confessionaries')
        logger.error(error)
        reject(error)
      })
  })
}
