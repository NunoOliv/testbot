const { loggers } = require('winston')
const { DiceRoll } = require('rpg-dice-roller')
const { MessageEmbed } = require('discord.js')

module.exports = {
  name: 'roll',
  description: 'Dice Roller!',
  command: true,
  execute (msg, args) {
    const logger = loggers.get(process.env.logger)
    const rollExp = args.join(' ')
    try {
      const roll = new DiceRoll(rollExp)
      msg.reply(rollerOutputMessage(roll))
    } catch (error) {
      switch (error.name) {
        case 'SyntaxError':
          msg.reply('The roll you typed is invalid, check the syntax and try again. For more information check https://greenimp.github.io/rpg-dice-roller/guide/notation/')
          break
        default:
          msg.reply('Something went wrong with your roll :/')
          logger.error(error)
      }
    }
  }
}

function rollerOutputMessage (roll) {
  const outputMessage = new MessageEmbed()
    .setTitle('Dice Roll')
    .setColor(0xff0000)
    .setDescription(roll.notation)
    .addFields(
      { name: 'Rolls', value: roll.rolls.join(' ') },
      { name: 'Result', value: '*' + roll.total + '*' }
    )

  return outputMessage
}
