'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('globalVars', [{
      varID: 'lastDay',
      groupID: 'covid',
      value: '0',
      status: 'active',
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    }, {
      varID: 'prefix',
      groupID: 'general',
      value: '!',
      status: 'active',
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    }], {})
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
}
